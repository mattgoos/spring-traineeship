appender("STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger - %msg%n"
    }
}

logger("org.axxes.traineeship", INFO)
root(INFO, ["STDOUT"])
