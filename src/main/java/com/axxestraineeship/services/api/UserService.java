package com.axxestraineeship.services.api;


public interface UserService {
  Integer registerUser(String name);

  void addToWishList(Integer userId, Integer movieId);

  void printWishList(Integer userId);
}
