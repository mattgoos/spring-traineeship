package com.axxestraineeship.services.api;

import com.axxestraineeship.dto.Movie;
import com.axxestraineeship.dto.Rating;
import com.axxestraineeship.dto.Review;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public interface MovieService {
  Movie findMovie(String titlePart);

  List<Review> getReviews(Integer movieId);

  Collection<Movie> getMovies();

  Rating rate(Integer userId, Integer movieId, Integer score, String comment);
}
