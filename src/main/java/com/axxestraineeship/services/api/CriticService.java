package com.axxestraineeship.services.api;

public interface CriticService {
  Integer registerCritic(String name);

  void review(Integer criticId, Integer movieId, Integer score, String review);

  void printReviews(Integer criticId);
}
