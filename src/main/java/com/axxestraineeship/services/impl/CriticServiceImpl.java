package com.axxestraineeship.services.impl;

import com.axxestraineeship.dao.api.CriticDao;
import com.axxestraineeship.dao.api.MovieDao;
import com.axxestraineeship.dao.api.ReviewDao;
import com.axxestraineeship.dto.Critic;
import com.axxestraineeship.dto.Movie;
import com.axxestraineeship.dto.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.axxestraineeship.services.api.CriticService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CriticServiceImpl implements CriticService {
  private Logger logger = LoggerFactory.getLogger(CriticService.class);

  private final ReviewDao reviewDao;
  private final CriticDao criticDao;
  private final MovieDao movieDao;

  @Autowired
  public CriticServiceImpl(CriticDao criticDao, ReviewDao reviewDao, MovieDao movieDao) {
    this.criticDao = criticDao;
    this.reviewDao = reviewDao;
    this.movieDao = movieDao;
  }

  @Override
  public Integer registerCritic(String name) {
     Critic critic = new Critic(name);
     return this.criticDao.insert(critic);
   }

  @Override
  public void review(Integer criticId, Integer movieId, Integer score, String reviewText) {
    getCritic(criticId);
    getMovie(movieId);
    Review review = new Review(criticId, movieId, score, reviewText);
    this.reviewDao.insert(review);
  }

  @Override
  public void printReviews(Integer criticId) {
    Critic critic = getCritic(criticId);
    List<Review> reviews = reviewDao.findByCritic(criticId);

    logger.info("Reviews written by critic {}:\n{}", critic.getName(),
      reviews.stream().map(review -> {
        Movie movie = getMovie(review.getMovieId());
        return "* " + movie + " - " + review;
      }).collect(Collectors.joining("\n")));
  }

  private Critic getCritic(Integer criticId) {
    Critic critic = criticDao.get(criticId);
    if (critic == null) {
      throw new RuntimeException("No critic found for " + criticId);
    }
    return critic;
  }

  private Movie getMovie(Integer movieId) {
    Movie movie = movieDao.get(movieId);
    if (movie == null) {
      throw new RuntimeException("No critic found for " + movieId);
    }
    return movie;
  }
}
