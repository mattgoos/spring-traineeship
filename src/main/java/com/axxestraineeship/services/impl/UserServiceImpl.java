package com.axxestraineeship.services.impl;

import com.axxestraineeship.dao.api.MovieDao;
import com.axxestraineeship.dao.api.UserDao;
import com.axxestraineeship.dto.Movie;
import com.axxestraineeship.dto.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.axxestraineeship.services.api.UserService;

@Service
public class UserServiceImpl implements UserService {
  private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

  private MovieDao movieDao;
  private UserDao userDao;

  @Autowired
  public UserServiceImpl(MovieDao movieDao, UserDao userDao) {
    this.movieDao = movieDao;
    this.userDao = userDao;
  }

  @Override
  public Integer registerUser(String name) {
    User user = new User(name);
    return userDao.insert(user);
  }

  @Override
  public void addToWishList(Integer userId, Integer movieId) {
    User user = getUser(userId, true);
    Movie movie = movieDao.get(movieId);
    user.addToWishList(movie);
  }

  @Override
  public void printWishList(Integer userId) {
    User user = getUser(userId, true);
    logger.info("Wishlist of user {}: {}", user.getName(), user.getWishList().stream().map(Movie::getTitle).toArray());
  }


  private User getUser(Integer userId) {
    return getUser(userId, false);
  }

  private User getUser(Integer userId, Boolean throwOnFail) {
    User user = userDao.get(userId);
    if (user == null && throwOnFail) {
      throw new RuntimeException("No user found for " + userId);
    }
    return user;
  }
}
