package com.axxestraineeship.services.impl;

import com.axxestraineeship.dao.api.BaseDao;
import com.axxestraineeship.dao.api.RatingDao;
import com.axxestraineeship.dao.api.MovieDao;
import com.axxestraineeship.dao.api.ReviewDao;
import com.axxestraineeship.dto.Movie;
import com.axxestraineeship.dto.Rating;
import com.axxestraineeship.dto.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.axxestraineeship.services.api.MovieService;
import java.util.Collection;
import java.util.List;


@Service
public class MovieServiceImpl implements MovieService {
  private Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);

  private final MovieDao movieDao;
  private final ReviewDao reviewDao;
  private final RatingDao ratingDao;

  @Autowired
  public MovieServiceImpl(MovieDao movieDao, RatingDao ratingDao, ReviewDao reviewDao) {
    this.movieDao = movieDao;
    this.ratingDao = ratingDao;
    this.reviewDao = reviewDao;
  }

  @Override
  public Movie findMovie(String title) {
    return movieDao.findByTitle(title);
  }

  @Override
  public List<Review> getReviews(Integer movieId) {

    return reviewDao.findByMovie(movieId);
  }

  @Override
  public Collection<Movie> getMovies() {
    return movieDao.all();
  }

  @Override
  public Rating rate(Integer userId, Integer movieId, Integer score, String comment) {
    Rating rating = new Rating(userId, movieId, comment, score);
    ratingDao.insert(rating);
    return rating;
  }
}
