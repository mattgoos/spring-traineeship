package com.axxestraineeship.dto;

public class Review extends BaseDto {
  private Integer criticId;
  private Integer rating;
  private Integer movieId;
  private String review;

  public Review(Integer criticId, Integer movieId, Integer rating, String review) {
    this.criticId = criticId;
    this.movieId = movieId;
    this.rating = rating;
    this.review = review;
  }

  public Integer getCriticId() {
    return criticId;
  }

  public Integer getRating() {
    return rating;
  }

  public String getReview() {
    return review;
  }

  public Integer getMovieId() {
    return this.movieId;
  }

  @Override
  public String toString() {
    return review + " (rating: " + rating + ")";
  }
}
