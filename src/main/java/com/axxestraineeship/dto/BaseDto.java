package com.axxestraineeship.dto;

public abstract class BaseDto {
  private Integer id;

  BaseDto() {
    this.id = null;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
