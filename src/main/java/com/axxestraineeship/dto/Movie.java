package com.axxestraineeship.dto;

public class Movie extends BaseDto {
  private String title;
  private String year;
  private String description;


  public Movie(String title, String year, String description) {
    super();
    this.title = title;
    this.year = year;
    this.description = description;
  }

  public String getTitle() {
    return title;
  }

  public String getYear() {
    return year;
  }

  public String getDescription() {
    return description;
  }

  public String toString() {
    return this.getTitle() + "(" + this.getYear() + ")";
  }
}
