package com.axxestraineeship.dto;

public class Rating extends BaseDto {
  private final Integer movieId;
  private final Integer userId;
  private String comment;
  private Integer score;

  public Rating(Integer userId, Integer movieId, String comment, Integer score) {
    this.userId = userId;
    this.movieId = movieId;
    this.comment = comment;
    this.score = score;
  }

  public Integer getUserId() {
    return userId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Integer getScore() {
    return score;
  }

  public void setScore(Integer score) {
    this.score = score;
  }

  public Integer getMovieId() {
    return movieId;
  }
}
