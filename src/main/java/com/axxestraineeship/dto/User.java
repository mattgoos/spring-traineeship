package com.axxestraineeship.dto;

import java.util.ArrayList;
import java.util.List;

public class User extends BaseDto {

  private String name;
  private List<Movie> wishList;

  public User(String name) {
    this.name = name;
    this.wishList = new ArrayList<>();
  }

  public String getName() {
    return name;
  }

  public void addToWishList(Movie movie) {
    this.wishList.add(movie);
  }

  public List<Movie> getWishList() {
    return wishList;
  }
}
