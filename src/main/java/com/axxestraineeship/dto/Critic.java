package com.axxestraineeship.dto;

public class Critic extends BaseDto {
  private String name;

  public Critic(String name) {
    super();
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
