package com.axxestraineeship;

import com.axxestraineeship.dto.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.axxestraineeship.services.api.CriticService;
import com.axxestraineeship.services.api.MovieService;
import com.axxestraineeship.services.api.UserService;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan
public class RottenPotatoes {
  private static final Logger logger = LoggerFactory.getLogger(RottenPotatoes.class);

  private static MovieService movieService;
  private static UserService userService;
  private static CriticService criticService;

  private static void getBeans(ApplicationContext ctx) {
    movieService = ctx.getBean(MovieService.class);
    userService = ctx.getBean(UserService.class);
    criticService = ctx.getBean(CriticService.class);
  }

  public static void main(String[] args) {
    logger.info("Starting application");
    ApplicationContext ctx = new AnnotationConfigApplicationContext(RottenPotatoes.class);
    getBeans(ctx);

    Integer userId = userService.registerUser("Matthias Goossens");
    Integer criticId = criticService.registerCritic("Angri Critique");

    Movie theMeg = movieService.findMovie("The Meg");
    Movie jaws = movieService.findMovie("Jaws");
    Movie sharknado = movieService.findMovie("Sharknado");

    movieService.rate(userId, sharknado.getId(), 3, "Mediocrity");

    criticService.review(criticId, jaws.getId(), 2, "More water than shark");
    criticService.review(criticId, sharknado.getId(), 5, "Tara Reid taking herself so serious it's funny");
    criticService.review(criticId, theMeg.getId(), 2, "Too much teeth in one movie");
    userService.addToWishList(userId, theMeg.getId());
    userService.addToWishList(userId, jaws.getId());
    userService.addToWishList(userId, sharknado.getId());

    userService.printWishList(userId);
    criticService.printReviews(criticId);
  }
}
