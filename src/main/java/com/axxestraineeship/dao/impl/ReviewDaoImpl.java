package com.axxestraineeship.dao.impl;

import com.axxestraineeship.dao.api.ReviewDao;
import com.axxestraineeship.dao.api.SimpleDB;
import com.axxestraineeship.dto.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ReviewDaoImpl extends BaseDaoImpl<Review> implements ReviewDao {

  @Autowired
  ReviewDaoImpl(SimpleDB<Review> db) {
    super(db, ReviewDaoImpl.class);
  }

  @Override
  public List<Review> findByCritic(Integer criticId) {
    return this.all().stream()
        .filter(review -> review.getCriticId().equals(criticId))
        .collect(Collectors.toList());
  }

  @Override
  public List<Review> findByMovie(Integer movieId) {
    return this.all().stream()
        .filter(review -> review.getMovieId().equals(movieId))
        .collect(Collectors.toList());
  }
}
