package com.axxestraineeship.dao.impl;

import com.axxestraineeship.dao.api.RatingDao;
import com.axxestraineeship.dao.api.SimpleDB;
import com.axxestraineeship.dto.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RatingDaoImpl extends BaseDaoImpl<Rating> implements RatingDao {

  @Autowired
  RatingDaoImpl(SimpleDB<Rating> db) {
    super(db, RatingDaoImpl.class);
  }
}
