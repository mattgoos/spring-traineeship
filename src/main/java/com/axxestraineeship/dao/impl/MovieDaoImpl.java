package com.axxestraineeship.dao.impl;

import com.axxestraineeship.dao.api.MovieDao;
import com.axxestraineeship.dao.api.SimpleDB;
import com.axxestraineeship.dto.Movie;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class MovieDaoImpl extends BaseDaoImpl<Movie> implements MovieDao, InitializingBean {

  @Override
  public void afterPropertiesSet() {
    List<Movie> movies = Arrays.asList(
        new Movie("Jaws", "1975", "Jaws is a 1975 American thriller film directed by Steven Spielberg and based on " +
            "Peter Benchley's 1974 novel of the same name"),
        new Movie("The Meg", "2018", "The Meg is a 2018 science fiction thriller film directed by Jon Turteltaub with" +
            " " +
            " a screenplay by Dean Georgaris, Jon Hoeber, and Erich Hoeber, loosely based on the 1997 book Meg: A " +
            "Novel of Deep Terror by Steve Alten."),
        new Movie("Sharknado", "2013", "Sharknado is a 2013 made-for-television science fiction disaster comedy film " +
            "about a waterspout that lifts sharks out of the ocean and deposits them in Los Angeles.")
    );
    movies.forEach(this::insert);
  }

  @Autowired
  MovieDaoImpl(SimpleDB<Movie> db) {
    super(db, MovieDaoImpl.class);
  }

  @Override
  public Movie findByTitle(String title) {
    return this.all().stream()
        .filter(movie -> movie.getTitle().equals(title))
        .findFirst().orElse(null);
  }
}
