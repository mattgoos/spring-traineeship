package com.axxestraineeship.dao.impl;

import com.axxestraineeship.dao.api.SimpleDB;
import com.axxestraineeship.dao.api.UserDao;
import com.axxestraineeship.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

  @Autowired
  public UserDaoImpl(SimpleDB<User> db) {
    super(db, UserDaoImpl.class);
  }
}
