package com.axxestraineeship.dao.impl;

import com.axxestraineeship.dao.api.BaseDao;
import com.axxestraineeship.dao.api.SimpleDB;
import com.axxestraineeship.dto.BaseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

abstract class BaseDaoImpl<T extends BaseDto> implements BaseDao<T> {
  private final SimpleDB<T> db;

  private final Logger logger;

  BaseDaoImpl(SimpleDB<T> db, Class clazz) {
    this.db = db;
    logger = LoggerFactory.getLogger(clazz);
  }

  @Override
  public Integer insert(T value) {
    db.insert(value);
    logger.debug("Inserting '{}' at '{}' in database", value, value.getId());

    return value.getId();
  }

  @Override
  public void remove(Integer key) {
    logger.debug("Removing '{}' from {} database", key);
    db.remove(key);
  }

  @Override
  public T get(Integer id) {
    logger.debug("Retrieving '{}' from {} database", id);
    return db.get(id);
  }

  @Override
  public Collection<T> all() {
    return db.all();
  }

}
