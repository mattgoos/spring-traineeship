package com.axxestraineeship.dao.impl;

import com.axxestraineeship.dao.api.CriticDao;
import com.axxestraineeship.dao.api.SimpleDB;
import com.axxestraineeship.dto.Critic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CriticDaoImpl extends BaseDaoImpl<Critic> implements CriticDao {

  @Autowired
  public CriticDaoImpl(SimpleDB<Critic> db) {
    super(db, CriticDaoImpl.class);
  }
}
