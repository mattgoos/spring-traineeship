package com.axxestraineeship.dao.db;

import com.axxestraineeship.dao.api.SimpleDB;
import com.axxestraineeship.dto.BaseDto;
import java.util.Collection;
import java.util.Map;

public abstract class MapDBImpl<T extends BaseDto> implements SimpleDB<T> {

  private Integer idCounter = 0;

  private final Map<Integer, T> db;

  protected MapDBImpl(Map<Integer, T> dataStructure) {
    this.db = dataStructure;
  }

  @Override
  public Integer insert(T value) {
    Integer key = idCounter++;
    value.setId(key);
    db.put(key, value);
    return key;
  }

  @Override
  public T get(Integer key) {
    return db.get(key);
  }

  public void remove(Integer key) {
    db.remove(key);
  }

  @Override
  public Collection<T> all() {
    return db.values();
  }
}
