package com.axxestraineeship.dao.db;

import com.axxestraineeship.dto.BaseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@PropertySource("hashdb.properties")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Primary
public class HashMapDBImpl<T extends BaseDto> extends MapDBImpl<T> implements InitializingBean {
  private final Logger logger = LoggerFactory.getLogger(HashMapDBImpl.class);

  private Environment env;

  @Value("${host}")
  String host;

  @Value("${username}")
  String username;

  String password;

  @Override
  public void afterPropertiesSet() {
    password = env.getProperty("password");
    this.logger.info("Connecting to {} with credentials {}:{}", host, username, password);
  }

  @Autowired
  public void setEnv(Environment env) {
    this.env = env;
  }

  protected HashMapDBImpl() {
    super(new HashMap<>());
  }


}
