package com.axxestraineeship.dao.db;

import com.axxestraineeship.dto.BaseDto;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.TreeMap;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TreeMapDBImpl<T extends BaseDto> extends MapDBImpl<T> {

  protected TreeMapDBImpl() {
    super(new TreeMap<>());
  }
}
