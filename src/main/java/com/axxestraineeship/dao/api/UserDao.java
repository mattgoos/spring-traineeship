package com.axxestraineeship.dao.api;

import com.axxestraineeship.dto.User;

public interface UserDao extends BaseDao<User> {}
