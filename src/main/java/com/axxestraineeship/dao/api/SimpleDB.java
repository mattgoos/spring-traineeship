package com.axxestraineeship.dao.api;

import com.axxestraineeship.dto.BaseDto;

import java.util.Collection;

public interface SimpleDB<T extends BaseDto> {
  Integer insert(T value);

  T get(Integer key);

  void remove(Integer key);

  Collection<T> all();
}
