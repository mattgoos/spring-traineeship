package com.axxestraineeship.dao.api;

import com.axxestraineeship.dto.Rating;

public interface RatingDao extends BaseDao<Rating>{ }
