package com.axxestraineeship.dao.api;

import com.axxestraineeship.dto.Movie;

public interface MovieDao extends BaseDao<Movie> {
  Movie findByTitle(String title);
}
