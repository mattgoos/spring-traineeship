package com.axxestraineeship.dao.api;

import com.axxestraineeship.dto.Critic;

public interface CriticDao extends BaseDao<Critic> {}
