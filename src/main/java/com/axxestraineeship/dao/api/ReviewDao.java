package com.axxestraineeship.dao.api;

import com.axxestraineeship.dto.Review;

import java.util.List;

public interface ReviewDao extends BaseDao<Review> {
  List<Review> findByCritic(Integer criticId);

  List<Review> findByMovie(Integer movieId);
}
